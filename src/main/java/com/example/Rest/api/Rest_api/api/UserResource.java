package com.example.Rest.api.Rest_api.api;

import com.example.Rest.api.Rest_api.command.UserCommand;
import com.example.Rest.api.Rest_api.domain.User;
import com.example.Rest.api.Rest_api.dto.UserDTO;

import com.example.Rest.api.Rest_api.dto.mapper.UserMapper;
import com.example.Rest.api.Rest_api.sevice.user.UserSevice;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.example.Rest.api.Rest_api.constants.ResoucePath.USERS;


import java.util.Optional;

@RestController
@RequestMapping(USERS)
@RequiredArgsConstructor
public class UserResource {

    private final UserSevice userSevice;
    private final UserMapper userMapper;


    @GetMapping("{id}")
    public ResponseEntity<UserDTO> getOne(@PathVariable String id) {

        User user = userSevice.getUserById(id);

        return  ResponseEntity.ok(userMapper.toUserDto(user));
    }

    @GetMapping
    public ResponseEntity<List<UserDTO>> get() {

        List<User> listUsers = userSevice.getAllUsers();

        return ResponseEntity.ok(userMapper.toUsersListDto(listUsers));
    }

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody final UserCommand userCommand) {

        userSevice.createUser(userCommand);

        return ResponseEntity.ok().build();

    }



    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable String id){

        userSevice.deleteUser(id);

        return ResponseEntity.ok().build();

    }

    @PutMapping("{id}")
    public ResponseEntity<Void> update(@PathVariable final String id, @RequestBody final UserCommand userCommand) {

        userSevice.updateUser(id, userCommand);
        return ResponseEntity.ok().build();
    }

}
