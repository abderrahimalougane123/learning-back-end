package com.example.Rest.api.Rest_api.sevice.user;

import com.example.Rest.api.Rest_api.command.UserCommand;
import com.example.Rest.api.Rest_api.domain.User;
import com.example.Rest.api.Rest_api.repository.UserRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImp implements UserSevice {

    private final UserRepository userRepository;

    @Override
    public void createUser(UserCommand userCommand) {

        User user = User.createUser(userCommand);
        userRepository.save(user);
        log.info("User created successfully");


    }



    @Override
    public void updateUser(String id, UserCommand userCommand) {

        User user = userRepository.findUserByDeletedFalse(id);

        user.updateUser(userCommand);
        userRepository.save(user);
        log.info("User updated successfully");

    }

    @Override
    public void deleteUser(String id) {

         User user = getUserById(id);
         user.delete();
         log.info("user with id {} has been deleted successfully",id);
         userRepository.save(user);

    }

    @Override
    public User getUserById(String id) {

        User user= userRepository.findUserByDeletedFalse(id);
        log.info("begin getting user with id {}",id);
        return user;
    }

    @Override
    public List<User> getAllUsers() {
        List<User> users =userRepository.findAllUsersByDeletedFalse();
        log.info("begin getting users");
        return users;
    }
}
