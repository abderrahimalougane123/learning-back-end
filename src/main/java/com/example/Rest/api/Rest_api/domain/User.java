package com.example.Rest.api.Rest_api.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.transaction.Transactional;

import org.hibernate.annotations.GenericGenerator;

import com.example.Rest.api.Rest_api.command.UserCommand;

import lombok.*;

@Table(name = "users")
@Entity
@Transactional
@Getter
@Setter
@NoArgsConstructor
public class User  extends BaseEntity {



    @Column(name = "email")
    private String email;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "password")
    private String password;

    public static User createUser(UserCommand userCommand) {

        User user = new User();
        user.email = userCommand.getEmail();
        user.firstName = userCommand.getFirstName();
        user.lastName = userCommand.getLastName();
        user.password = userCommand.getPassword();
        return user;
    }

    public void updateUser(UserCommand userCommand) {

        this.email = userCommand.getEmail();
        this.firstName = userCommand.getFirstName();
        this.lastName = userCommand.getLastName();
        this.password = userCommand.getPassword();

    }
    public void delete() {
        this.deleted = true;
    }

}
