package com.example.Rest.api.Rest_api.command;


import lombok.Getter;

@Getter
public class UserCommand {
    private  String firstName;
    private  String lastName;
    private  String email;
    private  String password;
}
