package com.example.Rest.api.Rest_api.sevice.user;

import java.util.List;

import com.example.Rest.api.Rest_api.command.UserCommand;
import com.example.Rest.api.Rest_api.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserSevice {

    void createUser(UserCommand userCommand);

    List<User> getAllUsers();

    void updateUser(String id, UserCommand userCommand);

    void deleteUser(String id);

    User getUserById(String id);
}
