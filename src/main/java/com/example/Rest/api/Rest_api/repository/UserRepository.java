package com.example.Rest.api.Rest_api.repository;

import com.example.Rest.api.Rest_api.domain.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Query(value = "select * from users where  id like :id and  deleted is false",nativeQuery = true)
    User findUserByDeletedFalse(@Param("id") String id);

    @Query(value = "select * from users where deleted is false",nativeQuery = true)
    List<User> findAllUsersByDeletedFalse();

}
