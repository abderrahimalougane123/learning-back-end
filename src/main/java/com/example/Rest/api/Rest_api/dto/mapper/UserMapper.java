package com.example.Rest.api.Rest_api.dto.mapper;

import com.example.Rest.api.Rest_api.domain.User;
import com.example.Rest.api.Rest_api.dto.UserDTO;
import org.mapstruct.Mapper;
import java.util.List;

@Mapper(componentModel = "spring")
public abstract class UserMapper {
    public abstract UserDTO toUserDto(User user);
    public abstract List<UserDTO> toUsersListDto(List<User> listUsers);

}